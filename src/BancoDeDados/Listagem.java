/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BancoDeDados;

/**
 *
 * @author aluno
 */

import Classes.Agendamento.test1;
import com.toedter.calendar.JCalendar;
import java.awt.Color;
import java.awt.Component;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author aluno
 */
public class Listagem {
    
    
    
    public void ListarAgendamentoCliente(JList lista){
         try {
             
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica4.agenda where numero is null";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 dlm.addElement("Hora da consulta :"+ rs.getString("hora")+", Dia : "+ rs.getString("dia")+
                         ", Mes:"+ rs.getString("mes")+ ", Ano:"+ rs.getString("ano")+ ", Atendente :"+ rs.getString("login"));
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
    public void listarSalariosAdmin(JList lista){
         try {
             int total = 0;
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica4.funcionario where caixa is not null";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 dlm.addElement("Quanto o "+rs.getString("nome")+" tem que receber :"+rs.getString("caixa"));
                 total += Integer.parseInt(rs.getString("caixa"));
             }
             dlm.addElement("Quanto a empresa recebeu no final do mês : "+total);
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
        
     }
     public void listar(JList lista){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 dlm.addElement(rs.getString(""));
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
      public void ListagemClienteInfo(JList lista,int numeroAt,JTextField nome,JTextField email,JTextField numero,JTextField telefone ,JTextField endereco,JTextField login ,JTextField cpf ,JPasswordField pas){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica4.cliente where numero = '"+numeroAt+"'";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 dlm.addElement("VOCÊ JA FALTOU :"+rs.getString("aviso"));
                 dlm.addElement("DOCUMENTAÇÃO NECESSARIA PARA A PROXIMA CONSULTA:"+rs.getString("documentacao"));
                 nome.setText(rs.getString("nome"));
                 email.setText(rs.getString("email"));
                 numero.setText(rs.getString("numero"));
                 telefone.setText(rs.getString("telefone"));
                 endereco.setText(rs.getString("endereco"));
                 login.setText(rs.getString("login"));
                 cpf.setText(rs.getString("cpf"));
                 pas.setText(rs.getString("nome"));
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
       public void ListagemFuncioInfo(JList lista,String LoginAt,JTextField nome,JTextField email,JTextField telefone ,JTextField endereco,JTextField login ,JTextField cpf ,JPasswordField pas){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica4.funcionario where login = '"+LoginAt+"'";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 dlm.addElement("Seu caixa total :"+rs.getString("caixa"));
                 nome.setText(rs.getString("nome"));
                 email.setText(rs.getString("email"));
                 telefone.setText(rs.getString("telefone"));
                 endereco.setText(rs.getString("endereco"));
                 login.setText(rs.getString("login"));
                 cpf.setText(rs.getString("cpf"));
                 pas.setText(rs.getString("nome"));
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
     public void listarHorariosMarcados(JList lista){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "select * from clinica4.agenda where numero is not null";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 dlm.addElement(rs.getString("login") +" : "+ rs.getString("hora") +" :   "+rs.getString("dia")+"/"+rs.getString("mes")+"/"+rs.getString("ano")+"-"+ rs.getString("numero")+"-");
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
    public int listagemSalario(String login){
         try {
             
             Connection c = conexao.ObterConexao();
             String SQL = "select * from clinica4.funcionario where login = '"+login+"'" ;
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                return rs.getInt("salario");
             }
            
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
         return 0;
     }
    
    public void ListagemHorariosCliente(JList lista,String dia,String mes, String ano){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica4.agenda";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 if(rs.getString("numero") == null && dia.equalsIgnoreCase(rs.getString("dia"))&& mes.equalsIgnoreCase(rs.getString("mes"))&& ano.equalsIgnoreCase(rs.getString("ano"))){
                 dlm.addElement(rs.getString("hora") + " ;"+ rs.getString("login"));
                 }
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
      public JCalendar CalendarCliente(JCalendar calendar){
         try {
             // Coisas para o JCalendar , como retirada do dia inserido pelo Usuario
             String mes = ""+(calendar.getMonthChooser().getMonth() + 1);
             String ano = ""+calendar.getYearChooser().getYear(); 
             JPanel jPanel = calendar.getDayChooser().getDayPanel();
             Component component[] = jPanel.getComponents();
             //Fim das coisas do JCalendar e inicio do acesso ao banco     
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica4.agenda";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                String month = rs.getString("mes");
                String yr = rs.getString("ano");
                int dia = Integer.parseInt(rs.getString("dia"));
                if(month.equalsIgnoreCase(mes) && yr.equalsIgnoreCase(ano)){
                    component[dia + 11].setBackground(Color.green);
                }
             }
             
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
         return calendar;
     }
      public JCalendar FiltragemCalendarCliente1(JCalendar calendar,String Parametro,String Variavel){
         try {
             // Coisas para o JCalendar , como retirada do dia inserido pelo Usuario
             Calendar cal = Calendar.getInstance();
             cal.set(Calendar.DAY_OF_MONTH,1);
             int offset = cal.get(Calendar.DAY_OF_WEEK);
             String mes = ""+(calendar.getMonthChooser().getMonth() + 1);
             String ano = ""+calendar.getYearChooser().getYear(); 
             JPanel jPanel = calendar.getDayChooser().getDayPanel();
             Component component[] = jPanel.getComponents();
             System.out.println(Parametro);
             for(int cont = 0 ;cont !=offset + 31;cont ++){
                 component[cont + offset].setBackground(Color.WHITE);
             }
             //Fim das coisas do JCalendar e inicio do acesso ao banco     
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica4.agenda where numero is null and '"+Variavel+"' = "+Parametro;
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                String month = rs.getString("mes");
                String yr = rs.getString("ano");
                int dia = Integer.parseInt(rs.getString("dia"));
                if(month.equalsIgnoreCase(mes) && yr.equalsIgnoreCase(ano)){
                    component[dia + 11].setBackground(Color.blue);
                }
             }
             
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
         return calendar;
     }
       public JCalendar FiltragemCalendarCliente2(JCalendar calendar,String Parametro,String Variavel,String Parametro1,String Variavel1){
         try {
             // Coisas para o JCalendar , como retirada do dia inserido pelo Usuario
             Calendar cal = Calendar.getInstance();
             cal.set(Calendar.DAY_OF_MONTH,1);
             int offset = cal.get(Calendar.DAY_OF_WEEK);
             String mes = ""+(calendar.getMonthChooser().getMonth() + 1);
             String ano = ""+calendar.getYearChooser().getYear(); 
             JPanel jPanel = calendar.getDayChooser().getDayPanel();
             Component component[] = jPanel.getComponents();
              for(int cont = 0 ;cont !=offset + 31;cont ++){
                 component[cont + offset].setBackground(Color.WHITE);
             }
             //Fim das coisas do JCalendar e inicio do acesso ao banco     
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica4.agenda where numero is null and '"+Variavel+"' = "+Parametro+" and '"+Variavel1+"' = "+Parametro1;
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                String month = rs.getString("mes");
                String yr = rs.getString("ano");
                int dia = Integer.parseInt(rs.getString("dia"));
                if(month.equalsIgnoreCase(mes) && yr.equalsIgnoreCase(ano)){
                    component[dia + 11].setBackground(Color.blue);
                }
             }
             
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
         return calendar;
     }
        public JCalendar FiltragemCalendarCliente3(JCalendar calendar,String Variavel,String Parametro,String Variavel1,String Parametro1,String Variavel2,String Parametro2){
         try {
             // Coisas para o JCalendar , como retirada do dia inserido pelo Usuario
              Calendar cal = Calendar.getInstance();
             cal.set(Calendar.DAY_OF_MONTH,1);
             int offset = cal.get(Calendar.DAY_OF_WEEK);
             String mes = ""+(calendar.getMonthChooser().getMonth() + 1);
             String ano = ""+calendar.getYearChooser().getYear(); 
             JPanel jPanel = calendar.getDayChooser().getDayPanel();
             Component component[] = jPanel.getComponents();
             //Fim das coisas do JCalendar e inicio do acesso ao banco     
             for(int cont = 0 ;cont !=offset + 31;cont ++){
                 component[cont + offset].setBackground(Color.WHITE);
             }
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica4.agenda where  '"+Variavel+"' = "+Parametro+" and '"+Variavel1+"' = "+Parametro1+" and '"+Variavel2+"' = "+Parametro2;
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                String month = rs.getString("mes");
                String yr = rs.getString("ano");
                int dia = Integer.parseInt(rs.getString("dia"));
                if(month.equalsIgnoreCase(mes) && yr.equalsIgnoreCase(ano)){
                    component[dia + 11].setBackground(Color.blue);
                }
             }
             
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
         return calendar;
     }
         public JCalendar FiltragemCalendarCliente4(JCalendar calendar,String Variavel,String Parametro,String Variavel1,String Parametro1,String Variavel2,String Parametro2,String Variavel3,String Parametro3){
         try {
             // Coisas para o JCalendar , como retirada do dia inserido pelo Usuario
              Calendar cal = Calendar.getInstance();
             cal.set(Calendar.DAY_OF_MONTH,1);
             int offset = cal.get(Calendar.DAY_OF_WEEK);
             String mes = ""+(calendar.getMonthChooser().getMonth() + 1);
             String ano = ""+calendar.getYearChooser().getYear(); 
             JPanel jPanel = calendar.getDayChooser().getDayPanel();
             Component component[] = jPanel.getComponents();
              for(int cont = 0 ;cont !=offset + 31;cont ++){
                 component[cont + offset].setBackground(Color.WHITE);
             }
             //Fim das coisas do JCalendar e inicio do acesso ao banco     
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica4.agenda where numero is null and '"+Variavel+"' = "+Parametro+" and '"+Variavel1+"' = "+Parametro1+" and '"+Variavel2+"' = "+Parametro2+" and '"+Variavel3+"' = "+Parametro3;
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                String month = rs.getString("mes");
                String yr = rs.getString("ano");
                int dia = Integer.parseInt(rs.getString("dia"));
                if(month.equalsIgnoreCase(mes) && yr.equalsIgnoreCase(ano)){
                    component[dia + 11].setBackground(Color.blue);
                }
             }
             
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
         return calendar;
     }
           public JCalendar FiltragemCalendarCliente5(JCalendar calendar,String Variavel,String Parametro,String Variavel1,String Parametro1,String Variavel2,String Parametro2,String Variavel3,String Parametro3,String Variavel4,String Parametro4){
         try {
             // Coisas para o JCalendar , como retirada do dia inserido pelo Usuario
              Calendar cal = Calendar.getInstance();
             cal.set(Calendar.DAY_OF_MONTH,1);
             int offset = cal.get(Calendar.DAY_OF_WEEK);
             String mes = ""+(calendar.getMonthChooser().getMonth() + 1);
             String ano = ""+calendar.getYearChooser().getYear(); 
             JPanel jPanel = calendar.getDayChooser().getDayPanel();
             Component component[] = jPanel.getComponents();
              for(int cont = 0 ;cont !=offset + 31;cont ++){
                 component[cont + offset].setBackground(Color.WHITE);
             }
             //Fim das coisas do JCalendar e inicio do acesso ao banco     
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica4.agenda where numero is null and '"+Variavel+"' = "+Parametro+" and '"+Variavel1+"' = "+Parametro1+" and '"+Variavel2+"' = "+Parametro2+" and '"+Variavel3+"' = "+Parametro3+" and '"+Variavel4+"' = "+Parametro4;
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                String month = rs.getString("mes");
                String yr = rs.getString("ano");
                int dia = Integer.parseInt(rs.getString("dia"));
                if(month.equalsIgnoreCase(mes) && yr.equalsIgnoreCase(ano)){
                    component[dia + 11].setBackground(Color.blue);
                }
             }
             
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
         return calendar;
     }
    public void ListarServiços(JList lista){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "select * from clinica4.servicos";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 dlm.addElement(rs.getString("nome")+" - " + rs.getString("cod"));
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
     public String listar1(int cod){
         try {
            
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica4.Servicos where cod ='"+cod+"' ";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                
                    return rs.getString("nome");    
                
             }
            
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
         return null;
     }
   
    public void ListarDeclaração_Funcioario(JList lista){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica2.agenda"
                     + "   where numero != null";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 dlm.addElement(rs.getString("funcionario"));
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
     public void ListarInformaçõesGerais(JList lista){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica2.Cliente";                   
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 dlm.addElement("Nome registrado: "+ rs.getString("nome"));
                 dlm.addElement("CPF registrado: "+ rs.getString("cpf"));
                dlm.addElement("Email registrado: "+ rs.getString("email"));
               dlm.addElement("telefone registrado: "+ rs.getInt("telefone"));
              dlm.addElement("Voçê não compareceu: "+ rs.getInt("aviso") +" vez(es)( Ao não comparecer 3 vezes , tomaram penalidades" ); 
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
      public void ListarSalario(JList lista){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "Select salario from clinica2.TabelFunc";                     
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 dlm.addElement(rs.getString("salario"));
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
      public void ListarSalarioFuncionario(JList lista){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "Select caixa from clinica2.funcionario";                     
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 dlm.addElement(rs.getString("caixa"));
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
       public void ListarVisualizar_Adminstrador(JList lista){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "select * from clinica4.agenda";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 if(rs.getString("numero") != null){
                    dlm.addElement(rs.getString("hora") + ":"+rs.getString("dia")+":"+rs.getString("mes")+" Com um clinte marcado nesse horario");
                 }else{ 
                    dlm.addElement(rs.getString("hora") + ":"+rs.getString("dia")+":"+rs.getString("mes"));
             
             }
            }
             
           
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
         public void ListarVisualizar_Funcionario(JList lista){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "select * from clinica2.agenda";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
                while(rs.next()){
                  if(rs.getString("numero") != null){
                    dlm.addElement(rs.getString("hora") + ":"+rs.getString("dia")+":"+rs.getString("mes")+" Com um clinte marcado nesse horario");
                 }else
             
                 {
                    dlm.addElement("Horas :"+ rs.getString("hora") + "Dia :"+rs.getString("dia")+"  Mes :"+rs.getString("mes") +"ano : " + rs.getString("ano"));
             }
             }
           
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
        public int LoginInicial(String email,String senha){
           int a = 0;
            try {
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica2.Cadastro";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                if(email.equalsIgnoreCase(rs.getString("login") )&& senha.equalsIgnoreCase(rs.getString("senha"))){
                    a =  rs.getInt("op");
                }
             }
             
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
         return a;
     }
        // Com esse codigo é retirado do banco o ultimo "codigo" da table Serviços
        public int listarCodigoServiço(){
         try {
             Connection c = conexao.ObterConexao();
             String SQL = "select * from clinica4.Servicos where cod = (select max(cod) from clinica4.Servicos);";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
             return Integer.valueOf(rs.getString("cod"));
             }
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
         return 0;
     }
        public int listarCodigoServiçoFuncionario(String nome){
         try {
            
             Connection c = conexao.ObterConexao();
             String SQL = "select * from clinica4.Servicos where nome = ('"+nome+"');";
            Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             
             while(rs.next()){
              
             return Integer.valueOf(rs.getString("cod"));
             }
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
         return 0;
     }
         public void BoxServicos(JComboBox combo){
        try {
            DefaultComboBoxModel m = new DefaultComboBoxModel();
            Connection c = conexao.ObterConexao();
            PreparedStatement p =  c.prepareStatement("Select * from clinica4.Serv_func");
            ResultSet rs = p.executeQuery();
            while(rs.next()){
               // Data_Serie a = new Data_Serie(rs.getString("nome_serie"),rs.getInt("data_de_estreia"));
               String name =  ListagemCodigos(rs.getInt("cod"));
               String a = name +" - "+ rs.getString("login");
               m.addElement(a);                
            }   
            combo.setModel(m);
        } catch (SQLException ex) {
            Logger.getLogger(Listagem.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
         public String ListagemCodigos(int cod){
        try {
            DefaultComboBoxModel m = new DefaultComboBoxModel();
            Connection c = conexao.ObterConexao();
            PreparedStatement p =  c.prepareStatement("Select * from clinica4.Servicos where cod = ('"+cod+"')");
            ResultSet rs = p.executeQuery();
            while(rs.next()){
               // Data_Serie a = new Data_Serie(rs.getString("nome_serie"),rs.getInt("data_de_estreia"));
               return  rs.getString("nome");   
            }   
           
        } catch (SQLException ex) {
            Logger.getLogger(Listagem.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
        public void teste(JComboBox combo,String nome){
        try {
            DefaultComboBoxModel m = new DefaultComboBoxModel();
            Connection c = conexao.ObterConexao();
            PreparedStatement p =  c.prepareStatement("Select * from clinica4.Serv_func where login = ('"+nome+"')");
            ResultSet rs = p.executeQuery();
            while(rs.next()){
               // Data_Serie a = new Data_Serie(rs.getString("nome_serie"),rs.getInt("data_de_estreia"));
                int cod = rs.getInt("cod");
                System.out.println("a");
                String a = listar1(cod);
                m.addElement(a);      
            }   
            combo.setModel(m);
        } catch (SQLException ex) {
            Logger.getLogger(Listagem.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        public void ListarAgendaMudanca(JList lista,int dia,int hora,int mes,String serv){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "select * from clinica4.agenda where dia = ('"+dia+"') and hora = ('"+hora+"') and mes = ('"+mes+"') and login = ('"+serv+"');";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
               dlm.addElement("Hora da consulta:"+ rs.getString("hora")+", Dia:"+ rs.getString("dia")+
                         ", Mes: "+ rs.getString("mes")+ ", Ano:"+ rs.getString("ano")+ ", Atendente :"+ rs.getString("login")); 
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
   /*
     public void opa(JList combo){
        try {
            DefaultListModel dfm = new DefaultListModel();
            Connection c = conexao.ObterConexao();
            PreparedStatement p =  c.prepareStatement("SELECT * FROM sc_clinica.clientes");
            ResultSet rs = p.executeQuery();
            while(rs.next()){
                Classeteste a = new Classeteste(rs.getInt("numero"));
                dfm.addElement(a);      
            }   
            combo.setModel(dfm);
        } catch (SQLException ex) {
            Logger.getLogger(Listagem.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    */
    }


