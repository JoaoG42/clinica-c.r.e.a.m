/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BancoDeDados;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;

/**
 *
 * @author Maria do Carmo
 */
public class Insercao {
    // Cadastrar Clientes
     public void InserirAgendamentoCliente(String day,String hora,String mes,String ano,String Funcionario,String numero){
        try {
            /*dia = integer
              mes = integer
              ano = integer 
              hora = integer
            */
            Connection c = conexao.ObterConexao();
            String SQL = " update clinica4.Agenda" +
                           " set numero = ?" +
                           " where dia = ? and hora = ? and ano = ? and mes = ? and login = ?";
            PreparedStatement s = c.prepareStatement(SQL);
            s.setString(1, numero);
            s.setString(2, (day));
            s.setString(3, (hora));
            s.setString(4, (ano));
            s.setString(5, (mes));
            s.setString(6, Funcionario);           
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void Inserir(){
        try {
            Connection c = conexao.ObterConexao();
            String SQL = "";
            PreparedStatement s = c.prepareStatement(SQL);
            s.setString(1,"");
            s.setInt(2, Integer.valueOf(2));                    
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
      public void UpdateInfoCliente(String numeroAt,String nome,String email,String numero,String telefone,String endereco,String login,String cpf,String senha){
        try {
            Connection c = conexao.ObterConexao();
            String SQL = "Update clinica4.cliente set nome = ?,email = ?,numero = ?,telefone = ?,endereco = ?,login = ?,cpf = ?,senha = ? where numero = ?";
            PreparedStatement s = c.prepareStatement(SQL);
            s.setString(1,nome);
            s.setString(2,email);
            s.setInt(3,Integer.parseInt(numero));
            s.setInt(4,Integer.parseInt(telefone));
            s.setString(5,endereco);
            s.setString(6,login);
            s.setString(7,cpf);
            s.setString(8,senha); 
            s.setString(9,numeroAt);
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
      public void UpdateInfoFuncio(String LoginAt,String nome,String email,String telefone,String endereco,String login,String cpf,String senha){
        try {
            Connection c = conexao.ObterConexao();
            String SQL = "Update clinica4.funcionario set nome = ?,email = ?,telefone = ?,endereco = ?,login = ?,cpf = ?,senha = ? where login = ?";
            PreparedStatement s = c.prepareStatement(SQL);
            s.setString(1,nome);
            s.setString(2,email);
            s.setInt(3,Integer.parseInt(telefone));
            s.setString(4,endereco);
            s.setString(5,login);
            s.setString(6,cpf);
            s.setString(7,senha); 
            s.setString(8,LoginAt);
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
     public void UpdateServiços(String nome,int preco,int cod){
        try {
            Connection c = conexao.ObterConexao();
            String SQL = "Update clinica4.servicos set nome = ?,preco = ? where cod = ?";
            PreparedStatement s = c.prepareStatement(SQL);
            s.setString(1,nome);
            s.setInt(2, preco); 
            s.setInt(3,cod);
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
        public void DeletServiços(int cod){
        try {
            Connection c = conexao.ObterConexao();
            String SQL = "Delete from clinica4.serv_func where cod = ?;" +
                         "Delete from clinica4.servicos where cod = ?;";
            PreparedStatement s = c.prepareStatement(SQL);
            s.setInt(1,cod);      
            s.setInt(2,cod);                    
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    } public void InserirServiçoFuncionario(int cod,String login){
        try {
            Connection c = conexao.ObterConexao();
            String SQL = "Insert Into clinica4.serv_func(login,cod) Values(?,?)";
            PreparedStatement s = c.prepareStatement(SQL);
            s.setString(1,login);
            s.setInt(2, cod);                    
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
      public void InserirCadastroFuncionario(String day,String hora,String mes,String ano,String Funcionario){
        try {
            /*dia = integer
              mes = integer
              ano = integer 
              hora = integer
            */
            Connection c = conexao.ObterConexao();
            String SQL = "Insert into clinica4.agenda(dia,hora,ano,'"+mes+"',login) VALUES (?,?,?,?) ";
            PreparedStatement s = c.prepareStatement(SQL);       
            s.setString(1, (day));
            s.setString(2, (hora));
            s.setString(3, (ano));
           
            s.setString(4, Funcionario);           
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void InserirDocumentaçãoCliente(String nome,String Cpf,String Telefone,String email,String endereco,String usuario){
        try {
            /*dia = integer
              mes = integer
              ano = integer 
              hora = integer
            */
            Random r = new Random();
            int numero = r.nextInt(2433298);
            String numero1 = numero +"";
            Connection c = conexao.ObterConexao();
            String SQL = "Insert into clinica2.Cliente(email,nome,numero,telefone,endereço,login,cpf) VALUES (?,?,?,?,?,?,?) ";
            PreparedStatement s = c.prepareStatement(SQL);       
            s.setString(1, email);  
            s.setString(2, nome);  
            s.setString(3, numero1);  
            s.setString(4, Telefone);
            s.setString(5, endereco);
            s.setString(6, usuario);
            s.setString(7,Cpf);  
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void InserirCadastroIncial(String Email,String Senha,int Tipo,String usuario){
        try {
            Connection c = conexao.ObterConexao();
            String SQL = "Insert into clinica2.cadastro(email,senha,op,login) VALUES(?,?,?,?) ";
            PreparedStatement s = c.prepareStatement(SQL);
            s.setString(1,Email);
            s.setString(2,Senha);
            s.setInt(3,Tipo);   
            s.setString(4,usuario);
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void InserirDeclaraçãoPresença(int declaração){
        try {
            Connection c = conexao.ObterConexao();
            String SQL = "Insert into clinica2.Cliente(aviso) VALUES(?)";
            PreparedStatement s = c.prepareStatement(SQL);
            s.setInt(1, declaração);                      
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void InserirServiçosAdmin(String serviço,String preç,int cod){
        try {
            System.out.println(serviço+preç+cod);
            Connection c = conexao.ObterConexao();
            String SQL = "Insert into Clinica4.Servicos(nome,cod,preco) VALUES (?,?,?)";
            PreparedStatement s = c.prepareStatement(SQL);
            s.setString(1,serviço);
            s.setInt(2, cod); 
            long a = Long.parseLong(preç);
            s.setLong(3,a);
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
    }





