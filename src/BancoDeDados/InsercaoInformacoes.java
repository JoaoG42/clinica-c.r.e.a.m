/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BancoDeDados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maria do Carmo
 */
public class InsercaoInformacoes {
    public void InserirServiçoFuncionario(int cod,String login){
        try {
            Connection c = conexao.ObterConexao();
            String SQL = "Insert Into clinica4.serv_func(login,cod) Values(?,?)";
            PreparedStatement s = c.prepareStatement(SQL);
            s.setString(1,login);
            s.setInt(2, cod);                    
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void InserirDocumentaçãoCliente(String nome,String Cpf,int Telefone,String email,String endereco,String usuario,String senha,String numero){
         /* nome = String nome
            Cpf = Integer Cpf
            Telefone = String Telefone
            Email = String Emails          
         */
         
        try {      
           
          
            Connection c = conexao.ObterConexao();
            String SQL = "Insert into clinica4.Cliente(email,nome,numero,telefone,endereco,login,cpf,senha) VALUES (?,?,?,?,?,?,?,?) ";
            PreparedStatement s = c.prepareStatement(SQL);       
            s.setString(1, email);  
            s.setString(2, nome);  
            s.setString(3, numero);  
            s.setInt(4, Telefone);
            s.setString(5, endereco);
            s.setString(6, usuario);
            s.setString(7,Cpf);  
            s.setString(8, senha);
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void UpdateSalario(String login,int caixa){
        try {
            Connection c = conexao.ObterConexao();
            String SQL = "update clinica4.Funcionario set caixa = ? where login = ?";
            PreparedStatement s = c.prepareStatement(SQL);
            s.setString(2,login);
            s.setInt(1,caixa);                    
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
     public void InserirDocumentaçãoFuncionario(String nome,String Cpf,int Telefone,String email,String endereco,String usuario,String senha){
         /* nome = String nome
            Cpf = Integer Cpf
            Telefone = String Telefone
            Email = String Emails          
         */  
        try {  
            Connection c = conexao.ObterConexao();
            String SQL = "Insert into clinica4.Funcionario(email,nome,telefone,endereco,login,cpf,senha) VALUES (?,?,?,?,?,?,?) ";
            PreparedStatement s = c.prepareStatement(SQL);       
            s.setString(1, email);  
            s.setString(2, nome);
            s.setInt(3, Telefone);
            s.setString(4, endereco);
            s.setString(5, usuario);
            s.setString(6,Cpf);  
            s.setString(7, senha);
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void UpdateAvisoCliente(int numero){
        try {
            Connection c = conexao.ObterConexao();
            String SQL = "update clinica4.cliente set aviso = (select aviso from clinica4.cliente where numero = '"+numero+"') + 1  where numero = '"+numero+"'";
            PreparedStatement s = c.prepareStatement(SQL);                  
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
    public void InserirCadastroIncial(String Email,String Senha,int Tipo,String usuario){
          /*Email = String Email
        Senha = String senha
        Nome = String nome
        telefone = Integer/tinyInt telefone
        Cpf = Caracter(11) CPF
        Tipo = integer de 0 a 2
        */
        try {
            Connection c = conexao.ObterConexao();
            String SQL = "Insert into clinica2.cadastro(email,senha,op,login) VALUES(?,?,?,?) ";
            PreparedStatement s = c.prepareStatement(SQL);
            s.setString(1,Email);
            s.setString(2,Senha);
            s.setInt(3,Tipo);   
            s.setString(4,usuario);
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void InserirDeclaraçãoPresença(int declaração){
       // Declaração é uma variavel que diz se o cliente compareceu a consulta que devia ( 0 = não , 1 = sim)
        try {
            Connection c = conexao.ObterConexao();
            String SQL = "Insert into clinica2.Cliente(aviso) VALUES(?)";
            PreparedStatement s = c.prepareStatement(SQL);
            s.setInt(1, declaração);                      
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void InserirServiçosAdmin(String serviço,String preç,int cod){
        try {
            System.out.println(serviço+preç+cod);
            Connection c = conexao.ObterConexao();
            String SQL = "Insert into Clinica4.Servicos(nome,cod,preco) VALUES (?,?,?)";
            PreparedStatement s = c.prepareStatement(SQL);
            s.setString(1,serviço);
            s.setInt(2, cod); 
            long a = Long.parseLong(preç);
            s.setLong(3,a);
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
